
public class Production extends Thread {
	
	private Controlleur controlleur;
	private int indexJournee;
	
	public Production(Controlleur a_controlleur) {
		super();
		this.controlleur = a_controlleur;
		this.indexJournee = 0;
	}

	public void run() {
		try {
			Recette recetteCourante = Recette.getRecetteByID(1);
			int QtyPattesRecette = recetteCourante.getQtyPattes();
			int QtyDessusRecette = recetteCourante.getQtyDessus();
			int QtyPeintureRecette = recetteCourante.getQtyPeinture();

			
			boolean stop = false;
			boolean startProd = false;
			while (!stop) {
				if (ProduitsFinis.getNombreTablesTotal() > 320) {
					startProd = false;
					while (!startProd) {
						if (ProduitsFinis.getNombreTablesTotal() < 320) {
							startProd = true;
						}
					}
				} else {
					int QtyPattes = Materiaux.getTotalPattes();
					int QtyDessus = Materiaux.getTotalDessus();
					int QtyPeinture = Materiaux.getTotalPeinture();
					if (QtyPattes > QtyPattesRecette) {
						if (QtyDessus > QtyDessusRecette) {
							if (QtyPeinture > QtyPeintureRecette) {
								for (int i = 0; i < 8; i++) {
									for (int j = 0; j < 4; j++) {
										QtyPattes = Materiaux.getTotalPattes();
										QtyDessus = Materiaux.getTotalDessus();
										QtyPeinture = Materiaux.getTotalPeinture();
										this.controlleur.produire(1);
										Thread.sleep(250);														
									}
								}
								this.controlleur.finJournee(this.indexJournee++);
								Thread.sleep(16000);
								this.controlleur.debutJournee(this.indexJournee);
							} else {
								Commandes.commanderPeinture(recetteCourante.getRecetteID());
							}
						} else {
							Commandes.commanderDessus(recetteCourante.getRecetteID());
						}
					} else {
						Commandes.commanderPattes(recetteCourante.getRecetteID());
					}					
				}
			}
		} catch (Exception e) {}
	}	
}
