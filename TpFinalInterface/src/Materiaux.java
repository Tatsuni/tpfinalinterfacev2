import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class Materiaux extends EntiteBD {
	
	private int QtyPattes;
	private int QtyDessus;
	private int QtyPeinture;
	
	public Materiaux(int a_ID, int a_QtyPattes, int a_QtyDessus, int a_QtyPeinture) {
		super(a_ID);
		this.QtyPattes = a_QtyPattes;
		this.QtyDessus = a_QtyDessus;
		this.QtyPeinture = a_QtyPeinture;
	}
	
	public static Materiaux getMateriauxTotaux() throws Exception {
		String sql = "SELECT * FROM Materiaux";
		Connection connection = ConnectionDbSingleton.getInstance();
		Statement stmnt = connection.createStatement();
		ResultSet rs = stmnt.executeQuery(sql);

		int ID = 0;
		int QtyPattes = 0;
		int QtyDessus = 0;
		int QtyPeinture = 0;

		while (rs.next()) {
			ID = rs.getInt("ID");
			QtyPattes = rs.getInt("QtyPattes");
			QtyDessus = rs.getInt("QtyDessus");
			QtyPeinture = rs.getInt("QtyPeinture");
		}

		Materiaux m = new Materiaux(ID, QtyPattes, QtyDessus, QtyPeinture);

		return m;
	}

	public static void ajouterPattes(int nbAjout) throws Exception {
		String sql = "SELECT QtyPattes, QtyDessus, QtyPeinture FROM Materiaux";
		Connection connection = ConnectionDbSingleton.getInstance();
		Statement stmnt = connection.createStatement();
		ResultSet rs = stmnt.executeQuery(sql);
		
		int QtyPattes = 0;
		int QtyDessus = 0;
		int QtyPeinture = 0;
		
		while (rs.next()) {
			QtyPattes = rs.getInt("Qtypattes");
			QtyDessus = rs.getInt("QtyDessus");
			QtyPeinture = rs.getInt("QtyPeinture");
		}
		
		int QtyPattesTotales = QtyPattes + nbAjout;
		
		update(QtyPattesTotales, QtyDessus, QtyPeinture);
	}
	
	public static void ajouterDessus(int nbAjout) throws Exception {
		String sql = "SELECT QtyPattes, QtyDessus, QtyPeinture FROM Materiaux";
		Connection connection = ConnectionDbSingleton.getInstance();
		Statement stmnt = connection.createStatement();
		ResultSet rs = stmnt.executeQuery(sql);
		
		int QtyPattes = 0;
		int QtyDessus = 0;
		int QtyPeinture = 0;
		
		while (rs.next()) {
			QtyPattes = rs.getInt("Qtypattes");
			QtyDessus = rs.getInt("QtyDessus");
			QtyPeinture = rs.getInt("QtyPeinture");
		}
		
		int QtyDessusTotal = QtyDessus + nbAjout;
		
		update(QtyPattes, QtyDessusTotal, QtyPeinture);
	}
	
	public static void ajouterPeinture(int nbAjout) throws Exception {
		String sql = "SELECT QtyPattes, QtyDessus, QtyPeinture FROM Materiaux";
		Connection connection = ConnectionDbSingleton.getInstance();
		Statement stmnt = connection.createStatement();
		ResultSet rs = stmnt.executeQuery(sql);
		
		int QtyPattes = 0;
		int QtyDessus = 0;
		int QtyPeinture = 0;
		
		while (rs.next()) {
			QtyPattes = rs.getInt("Qtypattes");
			QtyDessus = rs.getInt("QtyDessus");
			QtyPeinture = rs.getInt("QtyPeinture");
		}
		
		int QtyPeintureTotal = QtyPeinture + nbAjout;
		
		update(QtyPattes, QtyDessus, QtyPeintureTotal);
	}
	
	public static void soustraireMateriaux(int IDRecette) throws Exception {
		String sql = "SELECT QtyPattes, QtyDessus, QtyPeinture FROM Recette WHERE ID=" + Integer.toString(IDRecette);
		Connection connection = ConnectionDbSingleton.getInstance();
		Statement stmnt = connection.createStatement();
		ResultSet rs = stmnt.executeQuery(sql);
		
		int QtyPattesRecette = 0;
		int QtyDessusRecette = 0;
		int QtyPeintureRecette = 0;

		while (rs.next()) {
			QtyPattesRecette = rs.getInt("QtyPattes");
			QtyDessusRecette = rs.getInt("QtyDessus");
			QtyPeintureRecette = rs.getInt("QtyPeinture");
		}

		sql = "SELECT QtyPattes, QtyDessus, QtyPeinture FROM Materiaux";
		connection = ConnectionDbSingleton.getInstance();
		stmnt = connection.createStatement();
		rs = stmnt.executeQuery(sql);
		
		int QtyPattesMateriaux = 0;
		int QtyDessusMateriaux = 0;
		int QtyPeintureMateriaux = 0;

		while (rs.next()) {
			QtyPattesMateriaux = rs.getInt("QtyPattes");
			QtyDessusMateriaux = rs.getInt("QtyDessus");
			QtyPeintureMateriaux = rs.getInt("QtyPeinture");
		}

		if (
			QtyPattesMateriaux > QtyPattesRecette && QtyDessusMateriaux > QtyDessusRecette
			&& QtyPeintureMateriaux > QtyPeintureRecette
		) {
			int QtyPattesFinal = QtyPattesMateriaux - QtyPattesRecette;
			int QtyDessusFinal = QtyDessusMateriaux - QtyDessusRecette;
			int QtyPeintureFinal = QtyPeintureMateriaux - QtyPeintureRecette;
			update(QtyPattesFinal, QtyDessusFinal, QtyPeintureFinal);
		} else {
			throw new Exception();
		}

	}

	public static int getTotalPattes() throws Exception {
		String sql = "SELECT QtyPattes FROM Materiaux";
		Connection connection = ConnectionDbSingleton.getInstance();
		Statement stmnt = connection.createStatement();
		ResultSet rs = stmnt.executeQuery(sql);
		
		int QtyPattes = 0;

		while (rs.next()) {
			QtyPattes = rs.getInt("QtyPattes");
		}
		
		return QtyPattes;
	}
	
	public static int getTotalDessus() throws Exception {
		String sql = "SELECT QtyDessus FROM Materiaux";
		Connection connection = ConnectionDbSingleton.getInstance();
		Statement stmnt = connection.createStatement();
		ResultSet rs = stmnt.executeQuery(sql);
		
		int QtyDessus = 0;

		while (rs.next()) {
			QtyDessus = rs.getInt("QtyDessus");
		}
		
		return QtyDessus;
	}

	public static int getTotalPeinture() throws Exception {
		String sql = "SELECT QtyPeinture FROM Materiaux";
		Connection connection = ConnectionDbSingleton.getInstance();
		Statement stmnt = connection.createStatement();
		ResultSet rs = stmnt.executeQuery(sql);
		
		int QtyPeinture = 0;

		while (rs.next()) {
			QtyPeinture = rs.getInt("QtyPeinture");
		}
		
		return QtyPeinture;
	}
	
	private static void update(int QtyPattes, int QtyDessus, int QtyPeinture) throws Exception {
		String sql = "UPDATE Materiaux SET QtyPattes=?, QtyDessus=?, QtyPeinture=?";
		Connection connection = ConnectionDbSingleton.getInstance();
		
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setInt(1, QtyPattes);
		ps.setInt(2, QtyDessus);
		ps.setInt(3, QtyPeinture);
		
		int affectedRows = ps.executeUpdate();
		
		if (affectedRows == 0) {
			throw new Exception();
		}
	}	
}
