
public class Acheteur extends Thread {
	private Controlleur controlleur;
	
	public Acheteur(Controlleur a_controlleur) {
		super();
		this.controlleur = a_controlleur;
	}
	
	public void run() {
		boolean stopAchat = false;
		Client client = new Client(2, "Guy", "Bernard");
		try {
			while (!stopAchat) {
				Thread.sleep(9000);
				this.controlleur.acheter(1, client);
				Thread.sleep(15000);
			}
		} catch (Exception e) {}
	}
}
