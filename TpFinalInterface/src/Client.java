import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class Client extends EntiteBD {

	private String ClientName;
	private String ClientLastName;
	
	public Client()
	{}
	
	public Client(int a_ID, String a_ClientName, String a_ClientLastName) {
		super(a_ID);
		this.ClientName = a_ClientName;
		this.ClientLastName = a_ClientLastName;
	}
	
	public String getClientName() {
		return this.ClientName;
	}
	
	public String getClientLastName() {
		return this.ClientLastName;
	}
	
	public static Client getClientByID(int a_ID) throws Exception {
		String sql = "SELECT ID, ClientName, ClientLastName FROM Client WHERE ID=" + Integer.toString(a_ID);
		Connection connection = ConnectionDbSingleton.getInstance();
		Statement stmnt = connection.createStatement();
		ResultSet rs = stmnt.executeQuery(sql);
		
		int ID = 0;
		String clientName = "";
		String clientLastName = "";
		
		while (rs.next()) {
			ID = rs.getInt("ID");
			clientName = rs.getString("ClientName");
			clientLastName = rs.getString("ClientLastName");
		}
		
		Client c = new Client(ID, clientName, clientLastName);
		
		return c;
	}
	
	public static String getClientLastName(int a_ID) throws Exception {
		String sql = "SELECT ClientLastName FROM Client WHERE ID=" + Integer.toString(a_ID);
		Connection connection = ConnectionDbSingleton.getInstance();
		Statement stmnt = connection.createStatement();
		ResultSet rs = stmnt.executeQuery(sql);
		
		String clientLastName = "";
		
		while (rs.next()) {
			clientLastName = rs.getString("ClientLastName");
		}
		
		return clientLastName;
		
	}
	
	public void save() throws Exception {
		if (this.Tracked) {
			update();
		} else {
			insert();
			this.Tracked = true;
		}
	}
	
	private void update() throws Exception {
		String sql = "UPDATE Client SET ClientName=?, ClientLastName=? WHERE ID=?";
		Connection connection = ConnectionDbSingleton.getInstance();
		
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setString(1, this.ClientName);
		ps.setString(2, this.ClientLastName);
		ps.setInt(3, this.ID);
		
		int affectedRows = ps.executeUpdate();
		
		if (affectedRows == 0) {
			throw new Exception();
		}
	}
	
	private void insert() throws Exception {
		String sql = "INSERT INTO Client(ClientName, ClientLastName) VALUES(?, ?)";
		Connection connection = ConnectionDbSingleton.getInstance();
		
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.setString(1, this.ClientName);
		ps.setString(2, this.ClientLastName);
		
		int affectedRows = ps.executeUpdate();
		
		if (affectedRows == 0) {
			throw new Exception();
		}
		
		ResultSet rs = ps.getGeneratedKeys();
		
		if (rs.next()) {
			this.ID = rs.getInt("ID");
		} else {
			throw new Exception();
		}
	}
}
